from pymodbus.server.asynchronous import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from twisted.internet.task import LoopingCall
import logging
import random

# Configure logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)

VALUE_CO: int = 1
VALUE_DI: int = 2
VALUE_IR: int = 4
VALUE_HR: int = 3


# Thread which interrupts every N sec
def updating_writer(a: (ModbusServerContext)):
    context: ModbusSlaveContext = a[0]

    values1 = context.getValues(VALUE_CO, 1,     count=128)
    values2 = context.getValues(VALUE_DI, 10001, count=128)
    values3 = context.getValues(VALUE_IR, 30001, count=32)
    values4 = context.getValues(VALUE_HR, 40001, count=99)

    values1 = [random.randint(0,   1) for v in values1]
    values2 = [random.randint(0, 255) for v in values2]
    values3 = [random.randint(0, 255) for v in values3]
    values4 = [random.randint(0, 255) for v in values4]

    log.debug("VAL CO: " + str(values1))
    log.debug("VAL DI: " + str(values2))
    log.debug("VAL IR: " + str(values3))
    log.debug("VAL HR: " + str(values4))

    context.setValues(VALUE_CO,     1, values1)
    context.setValues(VALUE_DI, 10001, values2)
    context.setValues(VALUE_IR, 30001, values3)
    context.setValues(VALUE_HR, 40001, values4)


def start_server(addr: str, port: int):
    units: dict[int, ModbusSlaveContext] = {
        0: ModbusSlaveContext(
                co=ModbusSequentialDataBlock(1,     [0] * 128),
                di=ModbusSequentialDataBlock(10001, [0] * 128),
                ir=ModbusSequentialDataBlock(30001, [0] * 32),
                hr=ModbusSequentialDataBlock(40001, [0] * 9999)
            ),
        1: ModbusSlaveContext(
                co=ModbusSequentialDataBlock(1,     [0] * 128),
                di=ModbusSequentialDataBlock(10001, [0] * 128),
                ir=ModbusSequentialDataBlock(30001, [0] * 32),
                hr=ModbusSequentialDataBlock(40001, [0] * 9999)
            ),
        2: ModbusSlaveContext(
                co=ModbusSequentialDataBlock(1,     [0] * 128),
                di=ModbusSequentialDataBlock(10001, [0] * 128),
                ir=ModbusSequentialDataBlock(30001, [0] * 32),
                hr=ModbusSequentialDataBlock(40001, [0] * 9999)
        ),
    }

    context = ModbusServerContext(slaves=units, single=False)

    # Server's identity
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'Siemens'
    identity.ProductCode = 'SIMATIC'
    identity.MajorMinorRevision = 'S7-200'
    # identities unknwon
    # identity.VendorUrl = ''
    # identity.ProductName = ''
    # identity.ModelName = ''
    # identity.UserApplicationName = ''

    # Start an async loop to periodically alter the context
    loop = LoopingCall(f=updating_writer, a=(context))
    loop.start(5)

    # Start the server
    StartTcpServer(context, identity, address=(addr, port))


if __name__ == '__main__':
    start_server('localhost', 502)
