from time import sleep
from pymodbus.client.sync import ModbusTcpClient

client = ModbusTcpClient('localhost')

while True:
    result = client.read_coils(1, 120, unit=0)
    print(result.bits)
    sleep(3)
